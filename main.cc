#include<iostream>
#include<iomanip>
#include<stdlib.h>
#include<time.h>
using namespace std;

struct Result
{
	int minutes,seconds;
};

Result *RESULTS=NULL;
int NUMBER_RESULTS;
int MIN_TIME=0;
int MAX_TIME=59;

int GenerateNumber(int, int);
void PrintResult(int);
int FindNumberMaxResult();
int FindNumberMinResult();
Result FindNumberArifmeticResult();
void PrintByConditions(Result);
Result AskResult();
void GenerateResults();
void PrintResults();

int main()
{
	srand(time(NULL));
	NUMBER_RESULTS = GenerateNumber(25,0)+25;
	RESULTS = new Result[NUMBER_RESULTS];
	GenerateResults();
	PrintResults();
	
	int Number;
	Number = FindNumberMinResult();
	cout << "min ";
	PrintResult(Number);
	cout << endl;
	Number = FindNumberMaxResult();
	cout << "max ";
	PrintResult(Number);
	cout << endl;
	
	Result Temp;
	Temp = FindNumberArifmeticResult();
	cout << "arifmetic ";
	cout << setw(2) << Temp.minutes << ':' << setw(2) << Temp.seconds << endl;
	cout << endl;
	Temp = AskResult();
	PrintByConditions(Temp);
}

int GenerateNumber(int leftLimit, int rightLimit)
{
	return rand()%((rightLimit-leftLimit)-rightLimit);
}
void PrintResult(int Number)
{
	cout << setw(2) << RESULTS[Number].minutes << ':' << setw(2) << RESULTS[Number].seconds << endl;
}
int FindNumberMaxResult()
{
	int max_minutes = 0, max_seconds = 0, marker = 0;

	for (int i = 0; i < NUMBER_RESULTS; i++)
	{
			if (RESULTS[i].minutes > max_minutes)
		{
			max_minutes = RESULTS[i].minutes;
			max_seconds = RESULTS[i].seconds;
			marker = i;
		}
		if (RESULTS[i].minutes == max_minutes && RESULTS[i].seconds > max_seconds)
		{
			max_minutes = RESULTS[i].minutes;
			max_seconds = RESULTS[i].seconds;
			marker = i;
		}
	}
	return marker;
}
int FindNumberMinResult()
{
	int min_minutes = 59, min_seconds = 59, marker = 0;

	for (int i = 0; i < NUMBER_RESULTS; i++)
	{
			if (RESULTS[i].minutes < min_minutes)
		{
			min_minutes = RESULTS[i].minutes;
			min_seconds = RESULTS[i].seconds;
			marker = i;
		}
		if (RESULTS[i].minutes == min_minutes && RESULTS[i].seconds < min_seconds)
		{
			min_minutes = RESULTS[i].minutes;
			min_seconds = RESULTS[i].seconds;
			marker = i;
		}
	}
	return marker;
}
Result FindNumberArifmeticResult()
{
	Result someResult;
	for (int i = 0; i < NUMBER_RESULTS; i++)
	{
		someResult.minutes += RESULTS[i].minutes;
		someResult.seconds += RESULTS[i].seconds;
	}
	someResult.minutes /= NUMBER_RESULTS;
	someResult.seconds /= NUMBER_RESULTS;
	return someResult;
}
void PrintByConditions(Result someResult)
{
	for (int i = 0; i < NUMBER_RESULTS; i++)
	{
		if (RESULTS[i].minutes > someResult.minutes)
		{
			PrintResult(i);
		}
		if (RESULTS[i].minutes == someResult.minutes && RESULTS[i].seconds > someResult.seconds)
		{
			PrintResult(i);
		}
	}
}
Result AskResult()
{
	Result someResult;
	someResult.minutes = MAX_TIME+1;
	someResult.seconds = MAX_TIME+1;
	while (someResult.minutes<MIN_TIME or someResult.minutes>MAX_TIME or someResult.seconds<MIN_TIME or someResult.seconds>MAX_TIME)
	{
		cout << "Enter the time in the format MM: SS (minutes: seconds)" << endl;
		cout << "Enter minutes ";
		cin >> someResult.minutes;
		cout << "Enter seconds ";
		cin >> someResult.seconds;
		if (someResult.minutes<MIN_TIME or someResult.minutes>MAX_TIME or someResult.seconds<MIN_TIME or someResult.seconds>MAX_TIME)
			cout << "ERROR, try again" << endl;
	}
	return someResult;
}
void GenerateResults()
{
	for (int i=0;i<NUMBER_RESULTS;i++)
	{
		RESULTS[i].minutes = GenerateNumber(MAX_TIME,MIN_TIME);
		RESULTS[i].seconds = GenerateNumber(MAX_TIME,MIN_TIME);
	}
}
void PrintResults()
{
	for (int i = 0; i < NUMBER_RESULTS; i++)
	{
		PrintResult(i);
	}
}
